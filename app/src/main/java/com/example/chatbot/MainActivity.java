package com.example.chatbot;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.chatbot.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity implements MainPresenter.View {
    ActivityMainBinding binding;

    final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
    final Adapter adapter = new Adapter();
    final MainPresenter presenter = new MainPresenter(this);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        final Button button = binding.buttonSend;


        button.setOnClickListener(this::handleMessageSend);
        binding.recycler.setLayoutManager(linearLayoutManager);
        binding.recycler.setAdapter(adapter);
    }

    private void handleMessageSend(View view) {
        final TextView tv = binding.inputText;

        if (!tv.getText().toString().isEmpty()) {
            String message = tv.getText().toString();
            tv.setText("");
            presenter.onMessageSend(message);
            adapter.addMessage(new Msg(message));
        }
    }

    @Override
    public void onMessageSendSuccess(Msg message) {
        adapter.addMessage(message);
        linearLayoutManager.smoothScrollToPosition(binding.recycler, null,adapter.getItemCount());
    }

    @Override
    public void onMessageSendFailure(Msg msgError) {
        adapter.addMessage(msgError);
        linearLayoutManager.smoothScrollToPosition(binding.recycler, null,adapter.getItemCount());
    }
}