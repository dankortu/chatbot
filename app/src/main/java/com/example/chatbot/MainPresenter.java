package com.example.chatbot;


import androidx.annotation.NonNull;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainPresenter {

    private final View view;

    public MainPresenter(View view) {
        this.view = view;
    }

    public void onMessageSend(String message) {
        NetworkService.getInstance().getJSONApi().getResponse(message).enqueue(new Callback<Msg>() {
            @Override
            public void onResponse(@NonNull Call<Msg> call, @NonNull Response<Msg> response) {
                if (response.body() != null) {
                    Msg message = response.body();
                    message.setIsRequest(true);
                    view.onMessageSendSuccess(message);
                } else {
                    view.onMessageSendFailure(new Msg("ЧТО_ТО ПОШЛО НЕ ТАК").setIsRequest(true));
                }
            }

            @Override
            public void onFailure(@NonNull Call<Msg> call, @NonNull Throwable t) {
                view.onMessageSendFailure(new Msg("ЧТО_ТО ПОШЛО НЕ ТАК").setIsRequest(true));
            }
        });
    }

    public interface View {
        void onMessageSendSuccess(Msg message);
        void onMessageSendFailure(Msg msgError);
    }
}
