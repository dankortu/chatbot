package com.example.chatbot;


import android.graphics.Color;
import android.view.Gravity;


import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.example.chatbot.databinding.MessageBinding;

import static com.example.chatbot.R.drawable.req_bg;
import static com.example.chatbot.R.drawable.res_bg;


public class MessageViewHolder extends RecyclerView.ViewHolder {

    private final MessageBinding binding;

    public MessageViewHolder( @NonNull MessageBinding messageView) {
        super(messageView.getRoot());
        binding = messageView;
    }
    public void bind(Msg message) {

        binding.text.setText(message.getMessage());

        if (message.getIsRequest()){
            binding.text.setBackground(ContextCompat.getDrawable(binding.getRoot().getContext(), res_bg));
            binding.text.setTextColor(Color.WHITE);
            binding.message.setGravity(Gravity.START);
        } else {
            binding.text.setTextColor(Color.BLACK);
            binding.text.setBackground(ContextCompat.getDrawable(binding.getRoot().getContext(), req_bg));
            binding.message.setGravity(Gravity.END);
        }

    }
}
