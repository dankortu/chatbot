package com.example.chatbot;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Msg {
    @SerializedName("message")
    @Expose
    private final String message;

    private boolean isRequest = false;

    public Msg(String message) {
        this.message = message;
    }

    public Msg setIsRequest(boolean request) {
        isRequest = request;
        return this;
    }

    public boolean getIsRequest() {
        return isRequest;
    }

    public String getMessage() {
        return message;
    }
}
