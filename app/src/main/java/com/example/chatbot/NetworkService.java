package com.example.chatbot;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public final class NetworkService {
    private  static NetworkService mInstance;
    private  static final String BASE_URL = "http://192.168.3.61:3000";
    private final Retrofit mRetrofit;

    public static NetworkService getInstance() {

        synchronized (NetworkService.class){
            if (mInstance == null) {
                mInstance = new NetworkService();
            }
            return mInstance;
        }

    }

    public NetworkService() {

        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY );
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(loggingInterceptor);
        mRetrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .build();
    }

    public NodeServerApi getJSONApi() {
        return mRetrofit.create(NodeServerApi.class);
    }
}
