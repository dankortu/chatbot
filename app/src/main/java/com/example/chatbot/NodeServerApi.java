package com.example.chatbot;


import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface NodeServerApi {
    @GET("/")
    Call<Msg> getResponse(
            @Query("message") String message
    );
}
